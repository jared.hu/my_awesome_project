class User
	attr_reader :title
	attr_accessor :first_name, :last_name
	def initialize(first_name, last_name)
		@first_name = first_name
		@last_name = last_name
	end

	def title
		"Mr #{@last_name}"
	end
end

us = User.new("Jared", "Hu")
us.last_name = "Ong"
puts us.title